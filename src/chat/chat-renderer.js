// This file is required by the chat.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const app = require('electron').remote.app
const fs = require('fs')
const helpers = require('../css-helper.js')

const Config = require('electron-config');
const config = new Config();

const hpfy = require('../helpify/helpify.js');


usercss = app.getPath('documents') + "/Zwift/zwiftmap/user.css"

fs.access(usercss, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
    helpers.loadcss(usercss)
  }
})

let chatStyles = helpers.inlinecss()

let chat = { zoom: (config.has('chat.zoom') ? config.get('chat.zoom') : 100) };
changeZoom(chat.zoom)
console.log(`zoom is ${chat.zoom}`);

const {ipcRenderer} = require('electron')

// ipc messages to handle:

ipcRenderer.on('chat', (event, timestamp, user, message, firstName, lastName) => {
  console.log(timestamp, user, message, firstName, lastName) // prints message
  var username = `${firstName} ${lastName}`.trim();
  // $('#chat').prepend('<div class="chatmessage"><div class="timestamp">' + timestamp + '</div> <div class="user">' + (username ? username : user) + '</div><div class="message">' + message + '</div></div>')
  $('#chat').prepend(`<div class="chatmessage"><div class="timestamp">${timestamp}</div> <div class="${(username ? 'username' : 'user')}">${(username ? username : user)}</div><div class="message">${message}</div></div>`)
})

ipcRenderer.on('rideon', (event, timestamp, user, firstName, lastName) => {
  console.log(timestamp, user, 'rideon', firstName, lastName) // prints message
  var username = `${firstName} ${lastName}`.trim();
  // $('#chat').prepend('<div class="chatmessage"><div class="timestamp">' + timestamp + '</div> <div class="user">' + (username ? username : user) + '</div><div class="message">' + message + '</div></div>')
  $('#chat').prepend(`<div class="rideon"><div class="timestamp">${timestamp}</div> Ride On from <div class="${(username ? 'username' : 'user')}">${(username ? username : user)}</div></div>`)
})

ipcRenderer.on('ignore-mouse', (event, ignoreMouseEvents) => {
  if (ignoreMouseEvents) {
    // hide visual indicators
    $('html').removeClass('allow_interaction')
    } else {
      // show visual indicators that this window accepts mouse events
      $('html').addClass('allow_interaction')
    }
  })
  
  
  document.querySelector('#sample_text').addEventListener('click', () => {
    var timestamp = '10:15:0'
    var username = 'A. Zwift User Name';
    var message = 'Hello there. What are you up to? Is it raining? It used to never rain in Watopia but that is in the past. Bloody rain and thunder....'
    var usernameLong = 'A. Zwifter with a long User Name';
    var messageShort = 'Great...'
    
    for (i=0;i<9;i++) {
      $('#chat').prepend(`<div class="chatmessage"><div class="timestamp">${timestamp + i}</div> <div class="${(username ? 'username' : 'user')}">${(username ? username : user)}</div><div class="message">${message}</div></div>`)
      $('#chat').prepend(`<div class="chatmessage"><div class="timestamp">${timestamp + i}</div> <div class="${(usernameLong ? 'username' : 'user')}">${(usernameLong ? usernameLong : user)}</div><div class="message">${messageShort}</div></div>`)
    }
    $('#chat').prepend(`<div class="rideon"><div class="timestamp">${timestamp + i}</div> Ride On from <div class="${(username ? 'username' : 'user')}">${(username ? username : user)}</div></div>`)
})

document.querySelector('#remember').addEventListener('click', () => {
  chat.zoom = document.querySelector('#zoom').value;
  config.set('chat.zoom', chat.zoom);
  console.log(config.path, chat.zoom);
})

document.querySelector('#reset').addEventListener('click', () => {
  chat.zoom = 100;
  changeZoom(chat.zoom);
  document.querySelector('#zoom').value = chat.zoom;
})

function changeZoom(zoom) {
  chatStyles.innerHTML = `div.chatmessage, div.rideon { font-size: ${zoom}% }`;
}

document.querySelector('#zoom').addEventListener('input', () => {
  console.log('zoom', document.querySelector('#zoom').value )
  chat.zoom = document.querySelector('#zoom').value;
  changeZoom(chat.zoom);
})


// ---

window.addEventListener('load', () => {
    var elems = document.querySelectorAll('[data-help]')
    // var values =
    elems.forEach( (obj) => {
      // alert(obj.innerHTML);
      hpfy.__(obj);
      // return obj.value;
  });
})

