// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


const remote = require('electron').remote
const app = remote.app
const fs = require('fs')
const helpers = require('../css-helper.js')
const escapeHtml = require('escape-html')

const panzoom = require('pan-zoom');

const Config = require('electron-config');
const config = new Config();

const hpfy = require('../helpify/helpify.js');

let color = remote.getGlobal('color')
let track = remote.getGlobal('track')
console.log(track)

const statData = '?appplatform=' + process.platform + '&appversion=' + app.getVersion()
const mapOptions = '&background=0.8'

// attempt to load a css from the web
// this will help to overwrite css without deploying a new app version in case of breaking svg changes 
// helpers.loadcss('http://api.zwifthacks.com/zwiftmap/assets/world-macwin.css')

let colorStyles = helpers.inlinecss()
// now set color related styles as per configuration (global)
changeColor()

let roadStyles = helpers.inlinecss()
// now set color related styles as per configuration (global)
changeRoads()

let backgroundStyles = helpers.inlinecss()
// now set background related styles as per configuration (global)
changeBackground()

usercss = app.getPath('documents') + "/Zwift/zwiftmap/user.css"

fs.access(usercss, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
    helpers.loadcss(usercss)
  }
})

let state = {
  allow_interaction: false,
  allow_panzoom: false
}

// timer will be used to control display of buttons (when in window mode)
let timer = undefined;

// default world
let world = 1;

//
let map = document.querySelector('#map');
// svg root element
let svgroot
// svg g element for pan/zoom
let panzoomViewport 
// svg g element for centering
let centerViewport
// svg transforms for zoom
let map_before_scale
let map_scale
let map_after_scale
// svg transform for pan
let map_translate
// svg transform for follow me (centering)
let center_translate


// default transformation values for pan/zoom
let transforms = { 
  before_scale: {
    x:0,
    y:0
  },
  scale: {
    x:1, 
    y:1
  },
  translate: {
    x:0,
    y:0
  },
  follow_me: false
};

console.log(remote.getGlobal('color'))
console.log(colorStyles.innerHTML)

// the map is served from a web service
//$('#map').load('http://localhost:8000/zwiftmap/svg/world/')
// $('#map').load('http://api.zwifthacks.com/zwiftmap/svg/world/' + statData)
// $('#map').load('http://api.zwifthacks.com/zwiftmap/svg/world/' + statData + mapOptions, null , () => {
//   afterMapLoad();
// });


function afterMapLoad () {
  // assign object to global variable svgroot
  svgroot = document.querySelector('#map-svg');

  // inject g element for pan/zoom
  svgroot.innerHTML = '<g id="g_panzoom">' + svgroot.innerHTML + '</g>'

  // assign objects to global variables
  panzoomViewport = svgroot.querySelector('#g_panzoom');
  // centerViewport = svgroot.querySelector('#roads').parentElement;
  // Changed 2017-10-29 to also work when there is an image element (minimap)
  centerViewport = svgroot.querySelector('g.world');

  center_translate = svgroot.createSVGTransform();
  center_translate.setTranslate(0,0)
  centerViewport.transform.baseVal.appendItem(center_translate)

  // console.log(map);
  // console.log(svgroot);
  // console.log(svgroot.viewBox);
  // console.log(panzoomViewport);

  // determine world id from the svg
  for (var w=0; w<10; w++) {
    if (svgroot.classList.contains('world_' + w)) {
      world = w;
      break;
    }
  }
  console.log('Current world:', world)

  // override if pan/zoom values have been saved by the user for this world
  if (config.has('panzoom.' + world)) {
    transforms = config.get('panzoom.' + world);
  }
  // console.log(transforms)

  if (!transforms.before_scale) {
    transforms.before_scale = { x:0, y:0 };
  }
  
  if (!transforms.scale) {
    transforms.scale = { x:0, y:0 };
  }
  
  if (!transforms.translate) {
    transforms.translate = { x:0, y:0 };
  }
  
  if (transforms.follow_me == undefined) {
    transforms.follow_me = false;
  }

  var maptransform = panzoomViewport.transform.baseVal;
  if (maptransform.length == 0) {
      // console.log('no transform set in advance');
      // default zoom
      map_before_scale = svgroot.createSVGTransform();
      map_before_scale.setTranslate(transforms.before_scale.x, transforms.before_scale.y);
      maptransform.appendItem(map_before_scale)

      map_scale = svgroot.createSVGTransform();
      map_scale.setScale(transforms.scale.x, transforms.scale.y);
      maptransform.appendItem(map_scale)

      map_after_scale = svgroot.createSVGTransform();
      map_after_scale.setTranslate(-transforms.before_scale.x, -transforms.before_scale.y);
      maptransform.appendItem(map_after_scale)

      // default pan
      map_translate = svgroot.createSVGTransform();
      map_translate.setTranslate(transforms.translate.x, transforms.translate.y);
      maptransform.appendItem(map_translate)
  }


  // 


  $('#livedata').html(renderMarkers() + '<g id="livepositions" /><g id="activitypreview" />');
}


// 
panzoom(map, function (e) {
  if (!panzoomViewport) {
    // nothing to pan/zoom
    return;
  } 

  // force display of selected buttons (e.g. Reset)
  $('#controls').addClass('forced-display');
  // clear any old timer
  if (timer) { clearTimeout(timer); }
  // set new timer
  timer = setTimeout(() => { $('#controls').removeClass('forced-display'); }, 4000 );
  
  //e contains all the params related to the interaction

  //pan deltas
  // e.dx;
  // e.dy;
  // svg_w = panzoomViewport.ownerSVGElement.viewBox.baseVal.width;
  // svg_h = panzoomViewport.ownerSVGElement.viewBox.baseVal.height;
  svg_w = panzoomViewport.getBBox().width;
  svg_h = panzoomViewport.getBBox().height;

  var panning = 0;

  if ((map.clientHeight > map.clientWidth) && (svg_h > svg_w)) {
    // same orientation landscape / portrait of svg and window
    panning = minval(svg_h / map.clientHeight, svg_w / map.clientWidth)
  } else {
    panning = maxval(svg_h / map.clientHeight, svg_w / map.clientWidth)
  }

  map_translate.setTranslate(transforms.translate.x += (e.dx * panning / transforms.scale.x), transforms.translate.y += (e.dy *  panning / transforms.scale.y));

  //zoom delta
  // e.dz;
  // console.log(transforms.scale.x, e.dz);
  var scaling = e.dz/2000;

  transforms.scale.x -= scaling
  transforms.scale.y -= scaling

  svg_w = panzoomViewport.ownerSVGElement.viewBox.baseVal.width;
  svg_h = panzoomViewport.ownerSVGElement.viewBox.baseVal.height;
  svg_x = panzoomViewport.ownerSVGElement.viewBox.baseVal.x;
  svg_y = panzoomViewport.ownerSVGElement.viewBox.baseVal.y;
  // svg_w = panzoomViewport.getBBox().width;
  // svg_h = panzoomViewport.getBBox().height;
  // svg_x = panzoomViewport.getBBox().x;
  // svg_y = panzoomViewport.getBBox().y;

  transforms.before_scale.x = svg_x + (svg_w / 2); // center_x
  transforms.before_scale.y = svg_y + (svg_h / 2); // center_y

  // map_scale.setScale(transforms.scale.x -= scaling, transforms.scale.y -= scaling);
  map_before_scale.setTranslate(transforms.before_scale.x, transforms.before_scale.y)
  map_scale.setScale(transforms.scale.x, transforms.scale.y );
  map_after_scale.setTranslate(-transforms.before_scale.x, -transforms.before_scale.y)

  // console.log(panzoomViewport.transform)

  //coordinates of the center
  // e.x;
  // e.y;
  // console.log(e.x, e.y)

  //type of interaction: mouse, touch, keyboard
  // e.type;
  
  // console.log(e);

});




//
document.querySelector('#reset').addEventListener('click', () => {
  if (map_scale) {
    map_before_scale.setTranslate(0,0);
    map_scale.setScale(1,1);
    map_after_scale.setTranslate(0,0);
  }
  if (map_translate) {
    map_translate.setTranslate(0,0);
  }
  transforms.translate.x = transforms.translate.y = 0;
  transforms.before_scale.x = transforms.before_scale.y = 0;
  transforms.scale.x = transforms.scale.y = 1;

  renderFollowButton(transforms.follow_me = false);
  if (center_translate) {
    center_translate.setTranslate(0,0)
  }
})

//
document.querySelector('#panzoom').addEventListener('click', () => {
  if (! state.allow_panzoom) {
    // switch to allow panzoom
    $('html').addClass('allow_panzoom')
    if (map_scale) {
      map_before_scale.setTranslate(0,0);
      map_scale.setScale(1,1);
      map_after_scale.setTranslate(0,0);
    }
    if (map_translate) {
      map_translate.setTranslate(0,0);
    }
    //
    document.querySelector('#panzoom').innerHTML = 'Enable Drag Window'
  } else {
    $('html').removeClass('allow_panzoom')
    //
    document.querySelector('#panzoom').innerHTML = 'Enable Pan/Zoom'
  }
  // toggle state
  state.allow_panzoom = !state.allow_panzoom;
})

//
document.querySelector('#remember').addEventListener('click', () => {
  //
  // console.log(config.path)
  console.log('Will save config for world ' + world);
  config.set('panzoom.' + world, transforms);

})

document.querySelector('#follow-me').addEventListener('click', () => {
  if (transforms.follow_me) {
    transforms.follow_me = false;
  } else {
    transforms.follow_me = true;
  }
  renderFollowButton(transforms.follow_me)
})

function renderFollowButton(follow_me) {
  if (follow_me) {
    document.querySelector('#follow-me').innerHTML = '<i class="fa fa-dot-circle-o" aria-hidden="true"></i> Stop follow me';
  } else {
    document.querySelector('#follow-me').innerHTML = '<i class="fa fa-circle-o" aria-hidden="true"></i> Start follow me'
  }
}


const {ipcRenderer} = require('electron')

const powerMax = 750;
const powerColours = 6;
function getRiderClass(pos, index) {
  let powerIndex = Math.round(powerColours * pos.power / powerMax);
  return 'rider power-' + powerIndex + (pos.ghost ? ' rider-ghost' : '') + (pos.me ? ' me' : '');
}

function renderMarkers() {
  return '<defs><marker id="arrowHead" orient="auto" markerWidth="1" markerHeight="2" refX="0.1" refY="1">'
          + '<path d="M0,0 V2 L1,1 Z" />'
        + '</marker></defs>';
}

function renderPosition(pos, index) {
  if ((pos.me && track.me) || (!(pos.me) && track.followees)) {
    var name = (pos.me ? '' : pos.firstName.substring(0, 1) + ' ' + pos.lastName)
  
    return '<g class="' + getRiderClass(pos, index) + '" transform="translate(' + pos.x + ',' + pos.y + ')">'
    + (pos.trail ? renderTrail(pos) : '')
    + '<g transform="rotate(' + labelRotation + ')">'
    + '<circle cx="0" cy="0" r="6000">'
      + '<title>' + pos.power + 'w ' + Math.round(pos.speed / 1000000) + 'km/h</title>'
    + '</circle>'
    + '<text x="10000" y="3000">' + escapeHtml(name) + '</text>'
    + '</g ></g >';
  }
}

function renderTrail(pos) {
  const points = pos.trail.map(p => `${p.x - pos.x},${p.y - pos.y}`).join(' ');
    return '<polyline points="' + points + '" marker-end="url(#arrowHead)" />'
}

let labelRotation = 0;
function getLabelRotation() {
  const containers = $('#map .world g');
  const match = "rotate(";
  let found = false;

  containers.each(function (g) {
    if (!found) {
      const transform = this.attributes.transform ? this.attributes.transform.value : '';
      if (transform.substring(0, match.length) === match) {
        labelRotation = -parseInt(transform.substring(match.length));
        found = true;
      }
    }
  });
}

// ipc messages to handle:
// set-position, set-world,
ipcRenderer.on('set-position', (event, posJSON) => {
  getLabelRotation();
  var positions = JSON.parse(posJSON);
  var positionHtml = positions.map(renderPosition).join('');
  //console.log(arg) // prints message
  $('#livepositions').html(positionHtml)

  if (transforms.follow_me) {
    positions.map(centerMe);
  }
  
})
ipcRenderer.on('set-world', (event, message) => {
	$('#map').load('http://api.zwifthacks.com/zwiftmap/svg/world/' + message + statData + mapOptions, null, () => {
    afterMapLoad()
  })
	//$('#map').load('http://localhost:8000/zwiftmap/svg/world/' + message)
})

let activityInterval
ipcRenderer.on('preview-activity', (event, activityJSON) => {
  const displayActivity = JSON.parse(activityJSON);
  const points = displayActivity.positions.map(p => `${p.x},${p.y}`).join(' ');
  const current = displayActivity.positions[0]
    
  const activityHtml = '<g id="display-activity" class="display-activity">'
      + '<polyline points="' + points + '" />'
      + '<circle id="activityPoint" cx="' + current.x + '" cy="' + current.y + '" r="6000" />'
    + '</g>'

  $('#activitypreview').html(activityHtml)

  const activityPoint = $('#activityPoint');
  let activityIndex = -30;
  activityInterval = setInterval(() => {
    activityIndex++;
    if (activityIndex >= displayActivity.positions.length) {
      activityIndex = -30;
    }

    const point = displayActivity.positions[Math.max(0, activityIndex)]
    activityPoint.attr('cx', point.x);
    activityPoint.attr('cy', point.y);
  }, 15);
})
ipcRenderer.on('clear-activity', () => {
  $('#activitypreview').html('');
  if (activityInterval) {
    clearInterval(activityInterval);
    activityInterval = null;
  }
})

ipcRenderer.on('ignore-mouse', (event, ignoreMouseEvents) => {
  if (ignoreMouseEvents) {
    // hide visual indicators
    $('html').removeClass('allow_interaction');
    // remove any forced controls currently on a timer
    $('#controls').removeClass('forced-display');
    state.allow_interaction = false;
    state.allow_panzoom = false;
  } else {
    // show visual indicators that this window accepts mouse events
    $('html').addClass('allow_interaction');
    state.allow_interaction = true;
    state.allow_panzoom = false;
  }
})

ipcRenderer.on('change-color', (event) => {
  changeColor()
})

ipcRenderer.on('change-background', (event) => {
  changeBackground()
})

ipcRenderer.on('change-roads', (event) => {
  changeRoads()
})

ipcRenderer.on('change-trackers', (event) => {
  changeTrackers()
})

ipcRenderer.on('change-focus', (event) => {
  changeFocus()
})

// ---

window.addEventListener('load', () => {
    var elems = document.querySelectorAll('[data-help]')
    // var values =
    elems.forEach( (obj) => {
      // alert(obj.innerHTML);
      hpfy.__(obj);
      // return obj.value;
  });
})


// function definitions -> ***************************

function changeColor () {
  // change color styles as per configuration (global)
  color = remote.getGlobal('color')

  if (color['use-default-color']) {
    colorStyles.innerHTML = 'g.riders .me { }  g.roads .roadline { }'
  } else {
    colorStyles.innerHTML = 'g.rider circle { stroke: ' + color.rider + '; }'
            + ' g.rider text { fill: ' + color.rider + '; }'
            + ' g.rider.me circle { stroke: ' + color.me + '; fill: ' + color.me + '; }'
            + ' g.rider.me text { fill: ' + color.me + '; }'
            + ' g.roads .roadline { stroke: ' + color.map + ' !important; } '
  }
} // changeColor

function changeBackground () {
  // change background opacity as per configuration (global)
  backgroundStyles.innerHTML = '.minimap { opacity: ' + remote.getGlobal('background') + '; }'
} // changeBackground

function changeRoads () {
  hideRunningOnlyRoads = remote.getGlobal('roads')['hide-running-only-roads']
  if (hideRunningOnlyRoads) {
    roadStyles.innerHTML = '.running.no-cycling { display: none; }'
  } else {
    roadStyles.innerHTML = ''
  }
}

function changeTrackers () {
  // change color styles as per configuration (global)
  track = remote.getGlobal('track')
} // changeTrackers


//
function centerMe(pos, index) {
  if (pos.me) {
    
    if (!centerViewport) {
      // nothing 
      return;
    } 

    svg_w = centerViewport.getBBox().width;
    svg_h = centerViewport.getBBox().height;
    svg_x = centerViewport.getBBox().x;
    svg_y = centerViewport.getBBox().y;

    var panx = svg_x + (svg_w / 2) - pos.x
    var pany = svg_y + (svg_h / 2) - pos.y

    center_translate.setTranslate(panx, pany);

    // console.log(svg_x, svg_y, svg_w, svg_h, pos.x, pos.y)
    // console.log(centerViewport.getBBox())

    map_translate.setTranslate(0,0)

  }

}


function minval(a, b) {
  if (a<b) {
    return a;
  } else {
    return b;
  }
}


function maxval(a, b) {
  if (a>b) {
    return a;
  } else {
    return b;
  }
}