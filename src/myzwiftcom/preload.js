// preload.js
const { ipcRenderer } = require('electron')
const request = require('request');

window.sendToElectron= function (channel, message) {
  // send to main
  ipcRenderer.send(channel, message);
  // send to renderer (webview host)
  ipcRenderer.sendToHost(channel, message);
}

window.doStuff= function () {
  console.log('stuuuuuuuuuuuuuuuuuuff')
}


window.doOtherStuff= function () {
  console.log('other stuuuuuuuuuuuuuuuuuuff')
}

window.doLogin= function (usr, pwd) {
  u = document.querySelector('input#username');
  p = document.querySelector('input#password');
  s = document.querySelector('button#submit-button');
  if (u && p ) { 
    u.value = usr;
    p.value = pwd;
  } 
  if (s) {
    s.click(); 
    return 'clicked'
  } 
}


// for intercepting xhr responses
function addXMLRequestCallback(callback){
  var oldSend, i;
  if( XMLHttpRequest.callbacks ) {
      // we've already overridden send() so just add the callback
      XMLHttpRequest.callbacks.push( callback );
  } else {
      // create a callback queue
      XMLHttpRequest.callbacks = [callback];
      // store the native send()
      oldSend = XMLHttpRequest.prototype.send;
      // override the native send()
      XMLHttpRequest.prototype.send = function(){
          // process the callback queue
          // the xhr instance is passed into each callback but seems pretty useless
          // you can't tell what its destination is or call abort() without an error
          // so only really good for logging that a request has happened
          // I could be wrong, I hope so...
          // EDIT: I suppose you could override the onreadystatechange handler though
          for( i = 0; i < XMLHttpRequest.callbacks.length; i++ ) {
              XMLHttpRequest.callbacks[i]( this );
          }
          // call the native send()
          oldSend.apply(this, arguments);
      }
  }
}

// e.g.
addXMLRequestCallback( function( xhr ) {
  // console.log( xhr.responseText ); // (an empty string)
  xhr.onreadystatechange=function(){ if ( xhr.readyState == 4 && xhr.status == 200 ) { 
    // console.log(JSON.parse(xhr.responseText)); 
    // console.log(xhr.responseText); 
    console.log(xhr.responseURL);
    if (xhr.responseURL == 'https://us-or-rly101.zwift.com/api/profiles/me') {
      console.log(xhr.responseText);
      window.sendToElectron('profile', xhr.responseText);
    }
    // console.log(xhr); 
  } }
});

// addXMLRequestCallback( function( xhr ) {
//    console.dir( xhr ); // have a look if there is anything useful here
// });

