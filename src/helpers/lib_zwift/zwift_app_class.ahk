; $Date: 2016/11/27 10:10:17 $

; The class declaration follows after the global variable definition....

; Global variables (if any)
; --

; Class declarations

class ZwiftApp {
	
	Static LogFile := A_MyDocuments "\Zwift\Logs\log.txt"
	Static Window := "ahk_class GLFW30"
	Static CloseLauncherFile := ZwiftApp.pf86()  "\Zwift\CloseLauncher.exe"
	Static ZwiftLauncherFile := ZwiftApp.pf86()  "\Zwift\ZwiftLauncher.exe"
	Static ZwiftDir := ZwiftApp.pf86()  "\Zwift"
	

	pf86()
	{
		EnvGet, pf, ProgramFiles(x86)
		Return pf
	}

	Static X
	Static Y
	Static Width
	Static Height
		
	; Force a call to a class initialisation
	Static __force_initialize_class := ZwiftApp.__InitializeClass()
	
	__InitializeClass() {
		; add any necessary class initialisation here >>>
		ZwiftApp.Refresh()
		; <<<
		return 1
	}
	
	Refresh() {
		; refresh variables
		If ( WinExist(ZwiftApp.Window) ) {
			WinGetPos, tempX, tempY, tempW, tempH, % ZwiftApp.Window
			ZwiftApp.X:= tempX
			ZwiftApp.Y := tempY
			ZwiftApp.Width := tempW
			ZwiftApp.Height := tempH
		} else {
			ZwiftApp.X:= 0
			ZwiftApp.Y := 0
			ZwiftApp.Width := 0
			ZwiftApp.Height := 0
		}
	}
	
	GetUser() {
		Loop, % A_MyDocuments "\Zwift\cp\user*", 2
		{
			Return SubStr(A_LoopFileName, 5)
		}
	}
}


;;

;; tests for debug
if (1=2) {
	msgbox, % ZwiftApp.X " " ZwiftApp.Y " " ZwiftApp.Width " " ZwiftApp.Height
}
if (false) {
	msgbox, % ZwiftApp.GetUser()
}