// preload.js
const { ipcRenderer } = require('electron')

window.sendToElectron= function (channel) {
    console.log(channel)
  ipcRenderer.send(channel)
}

window.getProfile= function () {
    window.loadDoc('/profile/', 'profile');
}

window.loadDoc= function (url, channel) {
    var xhttp;
    xhttp=new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        ipcRenderer.sendToHost(channel, xhttp.responseText);
    }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  }

window.injectStyleLink= function (url) {
    head = document.getElementsByTagName('head')[0]; link = document.createElement('link'); link.type = 'text/css'; link.rel = 'stylesheet'; link.href = url; head.appendChild(link);
}


// ---------------------------------


// function doOnWheel(event) { 
//     console.log('wheel ', event.deltaY, document.querySelector('#app .zoom-area').style.cssText ) 
// }

// document.onwheel = doOnWheel;



// ----------------------------


var storedWorldId;
var storedPositionLength;
var discardedCountFirst = 0;
var discardedCountNoPos = 0;

const originalOpen = XMLHttpRequest.prototype.open;
XMLHttpRequest.prototype.open = function () {
  // console.log('request started!');
  
  this.addEventListener("readystatechange", (response) => {
      // console.log('readystatechange', arguments)

      if (this.readyState === 4) {

          // console.log(arguments[0], arguments[1])
          // console.log(this)
          if ((arguments[0] == 'GET') && (arguments[1] == '/world/')) {
              
              // modify response (shorten interval)
              
              // we need to store the original response before any modifications
              // because the next step will erase everything it had
              var original_responseText = response.target.responseText;
              var original_response = JSON.parse(original_responseText)
              
              if ((discardedCountFirst < 1) && (storedWorldId != undefined) && (original_response.worldId) && (storedWorldId != original_response.worldId)) {
                  // throw away the first response for a new world
                  discardedCountFirst += 1;
                  console.log('Discarded a response - the first response for a new world')
                  this.abort()
              } else if ((discardedCountNoPos < 1) && (storedPositionLength > 0) && (original_response.positions) && (original_response.positions.length == 0)) {
                  // throw away the first response without any positions
                  discardedCountNoPos += 1;
                  console.log('Discarded a response - the first response without any positions')
                  this.abort()
              } else {
                  discardedCountFirst = 0;
                  discardedCountNoPos = 0;

                  // here we "kill" the response property of this request
                  // and we set it to writable
                  Object.defineProperty(this, "responseText", { writable: true });
                  Object.defineProperty(this, "response", { writable: true });
      
                  // now we can make our modifications and save them in our new property
                  var modified_response = JSON.parse(original_responseText);
                  if (modified_response.interval) {
                      modified_response.interval = Math.trunc(Math.min(modified_response.interval, 3000))
                  }

                  if (modified_response.positions && modified_response.worldId) {
                      modified_response.positions = modified_response.positions.filter(p => p.world == modified_response.worldId)
                  }

                  this.responseText = JSON.stringify(modified_response);
                  this.response = modified_response;
                      
                  // this.abort()
  
                  // storedResponse = { ...this.response };
                  storedWorldId = modified_response.worldId;
                  storedPositionLength = (modified_response.positions ? modified_response.positions.length : 0);
              }
          } else if ((arguments[0] == 'POST') && (arguments[1] == '/world')) {
              // this is a user initiated change of world so clear the stored world id to make sure that the next GET response isn't discarded
              storedWorldId = undefined;
              storedPositionLength = undefined;
              discardedCountFirst = 0
              discardedCountNoPos = 0
          }
      }
  });
    
  // this.addEventListener('load', function() {
  //     // console.log('request completed!');
  //     // console.log('request completed!',this.responseURL, this.readyState, this.status); //will always be 4 (ajax is completed successfully)
  //     // console.log(this.responseText); //whatever the response was

  //     if (this.responseURL == 'https://www.zwiftgps.com/world/') {
  //         var pos = JSON.parse(this.responseText).positions;
  //         window.sendToMain('positions', pos);
  //         window.sendToWebview('positions', pos);

  //     } else if (this.responseURL == 'https://www.zwiftgps.com/profile/') {
  //         // console.log('Got a profile!!!');
  //         window.sendToMain('profile', this.responseText);
  //         window.sendToWebview('profile', this.responseText);
  //     }



  // });
  originalOpen.apply(this, arguments);
};
